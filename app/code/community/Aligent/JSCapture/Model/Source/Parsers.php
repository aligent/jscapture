<?php

class Aligent_JSCapture_Model_Source_Parsers {

    protected $_aOptions;

    public function toOptionArray() {

        if (!$this->_aOptions) {
            $this->_aOptions = array(
                array(
                    'label' => 'Full',
                    'value' => 'aligent_jscapture/parsers_full'
                ),
                array(
                    'label' => 'Separate',
                    'value' => 'aligent_jscapture/parsers_separate'
                ),
                array(
                    'label' => 'Minify',
                    'value' => 'aligent_jscapture/parsers_minify'
                ),
                array(
                    'label' => 'Minify Separate',
                    'value' => 'aligent_jscapture/parsers_minifySeparate'
                ),
            );
        }

        return $this->_aOptions;
    }

}
