<?php

class Aligent_JSCapture_Model_Group_Libraries extends Aligent_JSCapture_Model_Group_Abstract implements Aligent_JSCapture_Model_GroupInterface {

    const TYPE = 'libraries';

    public $scripts = array();

    public function canGroup($oScript)
    {
        if ($oScript->getAttributes()->getCaptureGroup(self::TYPE)) {
            return true;
        }

        if ($oScript->getAttributes()->getSrc()) {
            return true;
        }

        return false;
    }

    public function getParser()
    {
        return Mage::getModel('aligent_jscapture/parsers_full');
    }

}

