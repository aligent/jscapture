<?php

class Aligent_JSCapture_Model_Group_Templates extends Aligent_JSCapture_Model_Group_Abstract implements Aligent_JSCapture_Model_GroupInterface {

    const TYPE = 'templates';

    public $scripts = array();

    public function canGroup($oScript)
    {
        parent::canGroup($oScript);

        if ($oScript->getAttributes()->getType() == 'text/x-handlebars-template') {
            return true;
        }

        return false;
    }

    public function getParser()
    {
        return Mage::getModel('aligent_jscapture/parsers_full');
    }

}

