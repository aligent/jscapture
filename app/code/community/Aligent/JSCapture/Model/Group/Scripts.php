<?php

class Aligent_JSCapture_Model_Group_Scripts extends Aligent_JSCapture_Model_Group_Abstract implements Aligent_JSCapture_Model_GroupInterface {

    const TYPE = 'scripts';

    public $scripts = array();

    public function canGroup($oScript)
    {
        return true;
    }

    public function getParser()
    {
        $sScriptParser = Mage::getStoreConfig('aligent_jscapture/settings/script_parser');
        return Mage::getModel($sScriptParser);
    }

}

