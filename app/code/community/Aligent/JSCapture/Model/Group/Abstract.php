<?php

abstract class Aligent_JSCapture_Model_Group_Abstract implements Aligent_JSCapture_Model_GroupInterface {

    const TYPE = 'type';

    public function getType()
    {
        return static::TYPE;
    }

    public function addScript($oScript)
    {
        $sScriptName = $oScript->getAttributes()->getCaptureName();

        if ($sScriptName) {
            $this->scripts[$sScriptName] = $oScript;
        } else {
            $this->scripts[] = $oScript;
        }
    }

    public function canGroup($oScript)
    {
        if ($oScript->getAttributes()->getCaptureGroup(self::TYPE)) {
            return true;
        }
    }

    public function getOutput()
    {
        return $this->getParser()->parse($this->scripts);
    }

}
