<?php

class Aligent_JSCapture_Model_Parsers_Separate extends Aligent_JSCapture_Model_Parsers_Abstract implements Aligent_JSCapture_Model_ParserInterface {

    const TYPE = 'separate';

    public function parse($aScripts)
    {
        $bComments = (bool) Mage::getStoreConfig('aligent_jscapture/settings/script_comments');

        $sResponse = '<script>';
        $sResponse .= PHP_EOL;
        foreach ($aScripts as $oScript) {
            if ($bComments) {
                $sResponse .= '// ' . $this->getScriptName($oScript);
                $sResponse .= PHP_EOL;
            }
            $sResponse .= $this->parseScript($oScript);
            $sResponse .= PHP_EOL;
            $sResponse .= PHP_EOL;
        }
        $sResponse .= PHP_EOL;
        $sResponse .= '</script>';

        return $sResponse;
    }

    protected function parseScript($oScript)
    {
        return $oScript->getScript();
    }

}
