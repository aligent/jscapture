<?php

class Aligent_JSCapture_Model_Parsers_Minify extends Aligent_JSCapture_Model_Parsers_Abstract implements Aligent_JSCapture_Model_ParserInterface {

    const TYPE = 'minify';

    public function parse($aScripts)
    {
        $sScripts = '';

        foreach ($aScripts as $oScript) {
            $sScripts .= $this->parseScript($oScript) . PHP_EOL;
        }

        $oJsMin = Mage::getModel('aligent_jscapture/jSMin');

        $oJsMin->setInput($sScripts);

        $sResponse = '<script>';
        $sResponse .= $oJsMin->min();
        $sResponse .= '</script>';

        return $sResponse;
    }

    protected function parseScript($oScript)
    {
        return $oScript->getScript();
    }

}
