<?php

class Aligent_JSCapture_Model_Parsers_Full extends Aligent_JSCapture_Model_Parsers_Abstract implements Aligent_JSCapture_Model_ParserInterface {

    const TYPE = 'full';

    public function parse($aScripts)
    {
        $sResponse = '';

        foreach ($aScripts as $oScript) {
            $sResponse .= $this->parseScript($oScript);
            $sResponse .= PHP_EOL;
            $sResponse .= PHP_EOL;
        }

        return $sResponse;
    }

    protected function parseScript($oScript)
    {
        return $oScript->getTag();
    }

}
