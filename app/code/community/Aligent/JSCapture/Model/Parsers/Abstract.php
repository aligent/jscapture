<?php

abstract class Aligent_JSCapture_Model_Parsers_Abstract implements Aligent_JSCapture_Model_ParserInterface {

    public function getType()
    {
        return static::TYPE;
    }

    public function getScriptName($oScript)
    {
        return $oScript->getAttributes()->getCaptureName();
    }

}
