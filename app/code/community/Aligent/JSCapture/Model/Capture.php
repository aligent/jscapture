<?php

class Aligent_JSCapture_Model_Capture {

    // regex matches all script tags that have capture="true" || capture='true' || capture=true attribute
    // it captures the whole script tag, the tag attributes and the tag contents as separate variables for manipulation
    const REGEX = '/<script(.*capture=[\"\']?true[\"\']?.*?)>([\s\S.]*?)<\/script>/i';

    const LIBRARY = 'library';
    const TEMPLATE = 'template';
    const SCRIPT = 'script';

    // the groups that scripts will get captured into (order matters)
    public $aGroupModelAliases = array(
        'aligent_jscapture/group_libraries',
        'aligent_jscapture/group_templates',
        'aligent_jscapture/group_scripts'
    );

    public $aGroupModels = array();

    /**
     * Entry point to the script, fires on http_response_send_before magento event
     * Finds script tags based on the regex, strips them from the body of the html
     * and appends them to just before the closing body tag.
     * Works for:
     * Libraries: <script src="myLib.js"></script>
     * Templates: <script type="text/x-handlebars-template"></script>
     * Scripts: <script>var myScript = {};</script>
     *
     * @param $oEvent
     */
    public function go($oEvent)
    {
        // get config for enabled
        $bEnabled = (bool) Mage::getStoreConfig('aligent_jscapture/settings/enabled');

        // if not enabled don't proceed
        if (!$bEnabled) return;

        // get the response object
        $oResponse = $oEvent->getResponse();

        // get the body of the response (html)
        $sHtml = $oResponse->getBody();

        // get the matched scripts
        $aMatchedScripts = $this->findScripts($sHtml);

        // if there's no matches nothing has to be done so just return out
        if (!count($aMatchedScripts)) return;

        // strip the scripts from inside the body
        $sHtml = $this->stripScripts($sHtml);

        // parse the matches into a nice magento collection
        $cScripts = $this->parseMatches($aMatchedScripts);

        // get group singletons
        $this->getGroupSingletons();

        // group the scripts into dependency containers
        $this->groupScripts($cScripts);

        // parse all the matches into an output string
        $sOutput = $this->getOutput();

        // add the output to the html
        $sHtml = $this->addOutputToHtml($sHtml, $sOutput);

        // update the response body
        $oResponse->setBody($sHtml);
    }

    protected function getGroupSingletons()
    {
        foreach ($this->aGroupModelAliases as $sGroupModelAlias) {
            $this->aGroupModels[] = Mage::getSingleton($sGroupModelAlias);
        }
    }

    /**
     * Parses the html and returns all the scripts that match the regex
     *
     * @param $sHtml
     * @return string
     */
    protected function findScripts($sHtml)
    {
        $aResult = array();

        preg_match_all(self::REGEX, $sHtml, $aResult);

        return $aResult;
    }

    /**
     * Strips the scripts out of the body of the html
     *
     * @param string $sHtml
     * @return string
     */
    protected function stripScripts($sHtml)
    {
        return preg_replace(self::REGEX, '', $sHtml);
    }

    /**
     * Appends the output scripts to just before the body close tag
     *
     * @param $sHtml
     * @param $sOutput
     * @return string
     */
    protected function addOutputToHtml($sHtml, $sOutput)
    {
        $bComments = (bool) Mage::getStoreConfig('aligent_jscapture/settings/output_comment');

        $sFinalOutput = '';
        if ($bComments) {
            $sFinalOutput .= PHP_EOL;
            $sFinalOutput .= PHP_EOL;
            $sFinalOutput .= '<!-- ################################## -->';
            $sFinalOutput .= PHP_EOL;
            $sFinalOutput .= '<!-- ####     JS Capture Output     ### -->';
            $sFinalOutput .= PHP_EOL;
            $sFinalOutput .= '<!-- ################################## -->';
            $sFinalOutput .= PHP_EOL;
            $sFinalOutput .= PHP_EOL;
        }
        $sFinalOutput .= $sOutput;
        $sFinalOutput .= PHP_EOL;
        $sFinalOutput .= '</body>';


        return str_replace('</body>', $sFinalOutput, $sHtml);
    }

    /**
     * Parses the matched scripts into a varien data collection
     *
     * @param $aMatches
     * @return Varien_Data_Collection|void
     * @throws Exception
     */
    protected function parseMatches($aMatches)
    {
        // no matches found so no point in continuing
        if (!isset($aMatches[0])) return;

        // create a new magento collection
        $cMatches = new Varien_Data_Collection();

        // loop the matches and fill the magento collection with match objects
        for ($i = 0; $i < count($aMatches[0]); $i ++) {
            $oScript = new Varien_Object();

            // add the 3 capture groups from the regex
            $oScript->setTag(trim($aMatches[0][$i]));
            $oScript->setAttributes(trim($aMatches[1][$i]));
            $oScript->setScript(trim($aMatches[2][$i]));

            // parse the attributes into a nice format
            $this->parseAttributes($oScript);

            // add the item to the collection
            $cMatches->addItem($oScript);
        }

        // return the pretty collection of matches
        return $cMatches;
    }

    /**
     * Parses the captured attribute string into a varien object
     *
     * @param Varien_Object $oScript
     */
    protected function parseAttributes($oScript)
    {
        // get the attribute string
        $sAttributes = $oScript->getAttributes();

        $aAttributeMatches = array();

        // use regex to get all the attributes capturing their names and their values into separate arrays for manipulation
        preg_match_all('/(\S+)=["\']?((?:.(?!["\']?\s+(?:\S+)=|[>"\']))+.)["\']?/i', $sAttributes, $aAttributeMatches);

        // create a new varien object
        $oAttributes = new Varien_Object();

        // loop the attributes setting each one onto the varien object in $key (attribute name) => $value format
        for ($i = 0; $i < count($aAttributeMatches[0]); $i++) {
            $oAttributes->setData(str_replace('-', '_', $aAttributeMatches[1][$i]), $aAttributeMatches[2][$i]);
        }

        // return the pretty attributes
        $oScript->setAttributes($oAttributes);
    }

    protected function groupScripts($cScripts)
    {
        foreach ($cScripts as $oScript) {
            foreach ($this->aGroupModels as $oModel) {
                if ($oModel->canGroup($oScript)) {
                    $oModel->addScript($oScript);
                    break;
                }
            }
        }
    }

    /**
     * Parses the collection of matches into the html for output
     *
     * @return string
     */
    protected function getOutput()
    {
        $bComments = (bool) Mage::getStoreConfig('aligent_jscapture/settings/group_comment');
        $sResponse = '';

        // parse each of the capture type groups building the html to return
        foreach ($this->aGroupModels as $oGroupModel) {
            $sOutput = $oGroupModel->getOutput();
            if (!$sOutput) continue;
            if ($bComments) {
                $sResponse .= '<!-- #####    ' . ucfirst($oGroupModel->getType()) . '    ##### -->';
                $sResponse .= PHP_EOL;
            }
            $sResponse .= $oGroupModel->getOutput();
        }

        // return the html ready to be appended somewhere in the response
        return $sResponse;
    }

}
