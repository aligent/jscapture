<?php

interface Aligent_JSCapture_Model_GroupInterface {

    public function getType();
    public function canGroup($oScript);
    public function getParser();
    public function getOutput();

}
