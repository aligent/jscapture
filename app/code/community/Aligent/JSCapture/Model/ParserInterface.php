<?php

interface Aligent_JSCapture_Model_ParserInterface {

    public function parse($aData);
    public function getType();

}
