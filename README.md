# JS Capture

## Description

A Magento module for removing javascript from within templates and rendering it at the bottom of the page

## What it does

A script tag gets a capture=true attribute. The module listens for the magento event just before it sends
back the http response. It parses the body of the response and strips all the eligible script tags. It then
groups those script tags into 'libraries' (scripts with a src attribute) 'templates' (handle bars templates)
and 'scripts' all other scripts. Each group is then parsed and scripts can be minified or grouped into a single
script tag. All groups are then rendered just before the closing body tag in the order of libraries, 
templates, scripts.

## How to use it

Make sure it's enabled in your Magento System Config under:
System -> Config -> Aligent -> JS Capture -> Settings -> Enabled

On your normal script tag add a capture=true attribute:

```html
<script capture="true">
    console.log('This is my script');
</script>
```

Additionally you can give your script a name which will be printed with the script in the output if enabled.
Output comments are only available using the separate and minifed separate parsers. You also have to enable
the comments in the system config: 
System -> Config -> Aligent -> JS Capture -> Settings -> Script Comments

Names are UNIQUE so giving another script the same name will overwrite the previous one. This can be useful
for when a template may be rendered multiple times but should only produce one script.

```html
<script capture="true" capture-name="my-script">
    console.log('This is my script');
</script>
```

If you want to you can specify the group for a script manually. Available groups are 'libraries', 'templates' and 'scripts'

```html
<script capture="true" capture-name="my-script" capture-group="libraries">
    console.log('This is my script');
</script>
```

Each group model has a constant for it's type should you want to leverage that.

```php
<script capture="true" capture-name="my-script" capture-group="<?php echo Mage::getSingleton('aligent_jscapture/group_library')->getType() ?>">
    console.log('This is my script');
</script>
```

### Add script to capture via layout XML

You can add a javascript file to the head block using the normal layout XML method and have JS Capture capture it by appending a capture param as follows. 

`<action method="addItem"><type>skin_js</type><name>js/file.js</name><params>capture=true capture-group="libraries"</params></action>`

## Parsers

JS Capture comes with 4 parsers. Given the example below the docs will detail the output.

```html
<html>
    <body>
        <script capture="true" capture-name="Script 1">
            console.log('Hello');
            console.log('This is script 1');
        </script>
        
        <p>This is some html inbetween</p>
        
        <script capture="true" capture-name="Script 2">
            console.log('Hello');
            console.log('This is script 2');
        </script>
        
        <p>This is some html after</p>
    </body>
</html>
```

### Full

The full parser outputs each script wrapped in it's original script tag as it was captured. This is the parser that is used
for outputting libraries and templates.

```html
<html>
    <body>
        <p>This is some html inbetween</p>
        <p>This is some html after</p>
        
        <script capture="true" capture-name="Script 1">
            console.log('Hello');
            console.log('This is script 1');
        </script>
        
        <script capture="true" capture-name="Script 2">
            console.log('Hello');
            console.log('This is script 2');
        </script>
    </body>
</html>
```

### Separate

This collects all the scripts and outputs them as they were captured however they are all combined into a single script tag

```html
<html>
    <body>
        <p>This is some html inbetween</p>
        <p>This is some html after</p>
        
        <script>
            // Script 1
            console.log('Hello');
            console.log('This is script 1');
        
            // Script 2
            console.log('Hello');
            console.log('This is script 2');
        </script>
    </body>
</html>
```

### Minify

This minifies all the scripts into a single script tag

```html
<html>
    <body>
        <p>This is some html inbetween</p>
        <p>This is some html after</p>
        
        <script>console.log('Hello');console.log('This is script 1');console.log('Hello');console.log('This is script 2');</script>
    </body>
</html>
```

### Minify Separate

This minifies all the scripts into a single script tag but keeps them separated

```html
<html>
    <body>
        <p>This is some html inbetween</p>
        <p>This is some html after</p>
        
        <script>
        // Script 1
        console.log('Hello');console.log('This is script 1');
        // Script 2
        console.log('Hello');console.log('This is script 2');
        </script>
    </body>
</html>
```
